Cloned from https://github.com/tu-darmstadt-ros-pkg/hector_pseye_camera.

# Edited to capture images from /dev/video0 instead of /dev/video1 for the Panasonic Laptop (Glove Project).

# hector_pseye_camera
Camera driver (node and nodelet) for efficiently retrieving monochrome images from PS Eye cameras